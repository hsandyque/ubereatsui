package com.test.ubereatsui.model

data class Restaurant(val title:String, val image:String,
                      val waitingTime:String, val exTag:String,
                      val rating:Double, val ratingCount:Int)
