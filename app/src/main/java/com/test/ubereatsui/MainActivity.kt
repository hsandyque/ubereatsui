package com.test.ubereatsui

import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.test.ubereatsui.fragments.AccountFragment
import com.test.ubereatsui.fragments.OrderFragment
import com.test.ubereatsui.fragments.RestaurantFragment
import com.test.ubereatsui.fragments.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    RestaurantFragment.OnFragmentInteractionListener,
    SearchFragment.OnFragmentInteractionListener,
    OrderFragment.OnFragmentInteractionListener,
    AccountFragment.OnFragmentInteractionListener{

    val KEY_NAVI_ITEM_ID = "KEY_NAVI_ITEM_ID"

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        home_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.white))
        search_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.white))
        order_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.white))
        account_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.white))
        setFragment(item.itemId)
        when (item.itemId) {
            R.id.navigation_home -> {
                home_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.black))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                search_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.black))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_order -> {
                order_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.black))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_account -> {
                account_indicator.setBackgroundColor(ContextCompat.getColor(this,android.R.color.black))
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        setFragment(R.id.navigation_home)
    }

    override fun onStop() {
        super.onStop()
        nav_view.setOnNavigationItemReselectedListener(null)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val id = savedInstanceState.getInt(KEY_NAVI_ITEM_ID)
        nav_view.selectedItemId = id
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle, persistentState: PersistableBundle) {
        super.onRestoreInstanceState(savedInstanceState, persistentState)
        val id = savedInstanceState.getInt(KEY_NAVI_ITEM_ID)
        nav_view.selectedItemId = id
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(KEY_NAVI_ITEM_ID, nav_view.selectedItemId)
        super.onSaveInstanceState(outState)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        outState.putInt(KEY_NAVI_ITEM_ID, nav_view.selectedItemId)
        super.onSaveInstanceState(outState, outPersistentState)
    }

    private fun setFragment(id: Int) {
        var fragment: Fragment? = null

        when (id) {
            R.id.navigation_home -> {
                fragment = supportFragmentManager.findFragmentById(R.id.navigation_home)
                if(fragment == null) {
                    fragment = RestaurantFragment.newInstance()
                }
            }
            R.id.navigation_search -> {
                fragment = supportFragmentManager.findFragmentById(R.id.navigation_search)
                if(fragment == null) {
                    fragment = SearchFragment.newInstance()
                }
            }
            R.id.navigation_order -> {
                fragment = supportFragmentManager.findFragmentById(R.id.navigation_order)
                if(fragment == null) {
                    fragment = OrderFragment.newInstance()
                }
            }
            R.id.navigation_account -> {
                fragment = supportFragmentManager.findFragmentById(R.id.navigation_account)
                if(fragment == null) {
                    fragment = AccountFragment.newInstance()
                }
            }
        }

        val size = supportFragmentManager.fragments.size
        fragment?.let {
            if(size == 0) {
                supportFragmentManager.beginTransaction().add(R.id.host_fragment, it).commitAllowingStateLoss()
            } else {
                supportFragmentManager.beginTransaction().replace(R.id.host_fragment, it).commitAllowingStateLoss()
            }
        }
    }
}
