package com.test.ubereatsui.fragments

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.ubereatsui.R
import com.test.ubereatsui.adapter.RestaurantAdapter
import com.test.ubereatsui.viewmodel.RestaurantViewModel
import kotlinx.android.synthetic.main.fragment_restaurant.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RestaurantFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RestaurantFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RestaurantFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var mAdapter: RestaurantAdapter? = null
    val vm: RestaurantViewModel by lazy {
        ViewModelProviders.of(this).get(RestaurantViewModel::class.java) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restaurant, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).let {
            it.setSupportActionBar(toolbar)
            toolbar.title = getString(R.string.title_restaurant)
            setHasOptionsMenu(true)
        }
        store_rv.layoutManager = LinearLayoutManager(context)
        mAdapter = RestaurantAdapter()
        store_rv.adapter = mAdapter
        vm.getRestaurants().observe(this,
            Observer { results ->
                run {
                    mAdapter?.let {
                        it.setItems(ArrayList(results))
                    }
                }
        })
        vm.loadRestaurants()
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        if (activity is OnFragmentInteractionListener) {
            listener = activity
        } else {
            throw RuntimeException(activity.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance() = RestaurantFragment()
    }
}
