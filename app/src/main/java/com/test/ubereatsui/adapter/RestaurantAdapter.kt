package com.test.ubereatsui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.test.ubereatsui.model.Restaurant
import kotlinx.android.synthetic.main.list_item_restaurant.view.*

class RestaurantAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRestaurants = ArrayList<Restaurant>()
    private var mContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(com.test.ubereatsui.R.layout.list_item_restaurant, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mRestaurants.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = mRestaurants[position]
        Glide.with(holder.itemView.image).load(item.image).centerCrop().into(holder.itemView.image)
        holder.itemView.title.text = item.title
        holder.itemView.ex_tag.text = item.exTag
        holder.itemView.time.text = item.waitingTime
        if(item.ratingCount == 0) {
            holder.itemView.rating_layout.visibility = View.GONE
        } else {
            holder.itemView.rating.text = item.rating.toString()
            holder.itemView.rating_count.text = "(" + item.ratingCount + ")"
            holder.itemView.rating_layout.visibility = View.VISIBLE
        }
    }

    public fun setItems(restaurant: ArrayList<Restaurant>) {
        mRestaurants = restaurant
        notifyDataSetChanged()
    }
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}