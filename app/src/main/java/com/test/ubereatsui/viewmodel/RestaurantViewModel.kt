package com.test.ubereatsui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.ubereatsui.model.Restaurant

class RestaurantViewModel : ViewModel() {
    private lateinit var restaurants: MutableLiveData<List<Restaurant>>

    fun getRestaurants(): MutableLiveData<List<Restaurant>> {
        if(!::restaurants.isInitialized) {
            restaurants = MutableLiveData()
        }
        return restaurants
    }

    fun loadRestaurants() {
        var restaurantsList = arrayListOf<Restaurant>()
        restaurantsList.add(Restaurant("胖老爹炸雞南港", "https://cdn.walkerland.com.tw/images/upload/poi/p68174/m26248/18a2f604a146410af87db6a2c8b81ec46a5dc933.jpg", "15-20", "台灣美食", 4.5, 230))
        restaurantsList.add(Restaurant("麥當勞中和店", "https://www.mcdonalds.com.tw/content/tw/ch/promotion/news_cokeglass0605/_jcr_content/promolevel2image.img.jpg/1559536784075.jpg", "5-10", "美式美食", 4.8, 630))
        restaurantsList.add(Restaurant("創義麵信義店", "https://scontent.ftpe8-3.fna.fbcdn.net/v/t1.0-9/59704668_2306659479399729_58946124035129344_n.jpg?_nc_cat=111&_nc_ht=scontent.ftpe8-3.fna&oh=3a2ef6386424189a688d784bc90d4e46&oe=5D8C5D85", "30-45", "義式美食", 0.0, 0))
        restaurants.postValue(restaurantsList)
    }
}